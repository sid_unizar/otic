package translation;

import java.util.ArrayList;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

public class SPARQLSearches {

private static String sparqlEndpoint = "http://localhost:3030/apertium/query"; 
//private static String sparqlEndpoint = "http://linguistic.linkeddata.es/sparql/";

	public static String getSparqlEndpoint() {return sparqlEndpoint;};
	public static void setSparqlEndpoint(String endpoint) {SPARQLSearches.sparqlEndpoint = endpoint;};
	public SPARQLSearches (String endpoint){setSparqlEndpoint(endpoint);}	
	
	
	/**
	 * return the URI of the translation set of two given languages
	 * @param sourceLanguage
	 * @param targetLanguage
	 * @return
	 */
	public static String obtainTranslationSetFromLanguages(String sourceLanguage, String targetLanguage){		
		String translationSetURI = "";		
		// Create a new query
		String queryString = 
			" PREFIX vartrans: <http://www.w3.org/ns/lemon/vartrans#> " +
			" PREFIX lime: <http://www.w3.org/ns/lemon/lime#> " +
		    " SELECT DISTINCT ?transSet " +
			" WHERE { " +
			"  ?transSet a vartrans:TranslationSet ; " + 
			"     lime:language \"" + sourceLanguage + "\" ;" +
			"     lime:language \"" + targetLanguage + "\" ." +
			"}";
		Query query = QueryFactory.create(queryString);
		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.sparqlService(getSparqlEndpoint(), query);		
		ResultSet results = qe.execSelect();
		//Review results
		for ( ; results.hasNext() ; )    {		
			QuerySolution soln = results.nextSolution() ;
			translationSetURI = soln.get("transSet").toString();							
		}		
		// Important - free up resources used running the query
		qe.close();		
		return translationSetURI;		
	}
	
	/**
	 * returns the URI of the lexicon of a given language
	 * @param lang
	 * @return
	 */
	public static String obtainLexiconFromLanguage(String lang){		
		String lexiconURI = "";		
		//TODO this query should  be restricted to a specific graph (e.g., Apertium) 		
		// Create a new query
		String queryString = 
			" PREFIX lime: <http://www.w3.org/ns/lemon/lime#> " +
		    " SELECT DISTINCT ?lexicon " +
			" WHERE { " +
			"  ?lexicon a lime:Lexicon ; " + 
			"     lime:language \"" + lang + "\" ."  +
			"}";
		Query query = QueryFactory.create(queryString);
		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.sparqlService(getSparqlEndpoint(), query);		
		ResultSet results = qe.execSelect();
		//Review results
		for ( ; results.hasNext() ; )    {  		
			QuerySolution soln = results.nextSolution() ;
			lexiconURI = soln.get("lexicon").toString();							
		}		
		// Important - free up resources used running the query
		qe.close();		
		return lexiconURI;		
	}

	/**
	 * returns the URIs of the lexical entries that correspond to a direct translation of the given lexical entry in the given dictionary
	 * @param translationSetURI
	 * @param sourceLexicalEntryURI
	 * @return 
	 */
	public static  ArrayList<String> obtainDirectTranslationsFromLexicalEntry(String translationSetURI, String sourceLexicalEntryURI){		
			ArrayList<String> translationLexicalEntries = new ArrayList<String>();		
			// Create a new query
			String queryString = 
				" PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#> " +
				" PREFIX vartrans: <http://www.w3.org/ns/lemon/vartrans#> " +
			    " SELECT DISTINCT ?lex_entry_translated " +
				" WHERE { " +
				"  { <" + translationSetURI + "> vartrans:trans ?trans ." + 
				"    ?trans  vartrans:source  ?sense_a . " +
				"    ?trans  vartrans:target  ?sense_b . " +
				"    ?sense_a ontolex:isSenseOf  <" + sourceLexicalEntryURI +"> ." +
				"    ?sense_b ontolex:isSenseOf  ?lex_entry_translated . " +
				"  } UNION " +
				"  { <" + translationSetURI + "> vartrans:trans ?trans ." + 
				"    ?trans  vartrans:source  ?sense_a . " +
				"    ?trans  vartrans:target  ?sense_b . " +
				"    ?sense_a ontolex:isSenseOf  ?lex_entry_translated . " +
				"    ?sense_b ontolex:isSenseOf  <" + sourceLexicalEntryURI +"> ." +
				"  } " +
   				"}";
			Query query = QueryFactory.create(queryString);
			// Execute the query and obtain results
			QueryExecution qe = QueryExecutionFactory.sparqlService(getSparqlEndpoint(), query);			
			ResultSet results = qe.execSelect();
			//Review results
			for ( ; results.hasNext() ; ) {			
				QuerySolution soln = results.nextSolution() ;
				//extract lexical entries of direct translations
				String translationLexicalEntry = new String(soln.get("lex_entry_translated").toString());				
				translationLexicalEntries.add(translationLexicalEntry);								
			}			
			// Important - free up resources used running the query
			qe.close();			
			return translationLexicalEntries;		
	}

	/**
	 * Returns the URIs of the lexical entries that correspond to a direct translation of the given lemma in the given dictionary and source language
	 * [older version for lemon (not Ontolex-lemon), pending to be updated or removed. However it is not currently used for OTIC calculation] 
	 * 
	 * @param translationSetURI
	 * @param sourceLemma
	 * @param sourceLanguage
	 * @return
	 */
  /*  public static  ArrayList<String> obtainDirectTranslationsFromLemma(String translationSetURI, String sourceLemma, String sourceLanguage){		
		ArrayList<String> translationLexicalEntries = new ArrayList<String>();	
		// Create a new query
		String queryString = 
			" PREFIX lemon: <http://www.lemon-model.net/lemon#> " +
			" PREFIX tr: <http://purl.org/net/translation#> " +
			" PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#> " +
		    " SELECT DISTINCT ?lex_entry_b " +
			" WHERE { " +
			"  ?form_a lemon:writtenRep \"" + sourceLemma + "\"@" + sourceLanguage + "." +
			"  ?lex_entry_a lemon:lexicalForm ?form_a . " +
			" <" + translationSetURI + "> tr:trans ?trans ." + 
			"  ?trans  tr:translationSense  ?sense_a . " +
			"  ?trans  tr:translationSense  ?sense_b . " +
			"  ?sense_a lemon:isSenseOf  ?lex_entry_a ." +
			"  ?sense_b lemon:isSenseOf  ?lex_entry_b . " +
		//	"  ?lex_entry_b lemon:lexicalForm ?form_b . " +
		//	"  ?form_b lemon:writtenRep ?translated_written_rep . " +
		//	"  ?lex_entry_b  lexinfo:partOfSpeech ?pos . " +
			"  ?lexicon_b lemon:entry ?lex_entry_b . " +
			" MINUS {?lexicon_b lemon:language \"" + sourceLanguage +"\"} . " +
			"}";
		Query query = QueryFactory.create(queryString);
		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.sparqlService(getSparqlEndpoint(), query);		
		ResultSet results = qe.execSelect();
		//Review results
		for ( ; results.hasNext() ; )    {		
			QuerySolution soln = results.nextSolution() ;
			//extract lexical entries of direct translations
			String translationLexicalEntry = new String(soln.get("lex_entry_b").toString());			
			translationLexicalEntries.add(translationLexicalEntry);							
		}  		
		// Important - free up resources used running the query
		qe.close();		
		return translationLexicalEntries;	
	}
*/

	/**
	 * returns the URI of a lexical entry associated to a given lemma in a given lexicon
	 * @param translationSetURI
	 * @param sourceLexicalEntryURI
	 * @return
	 */
	public static  ArrayList<String> obtainLexicalEntriesFromLemma(String lexiconURI, String lemma, String sourceLanguage){		
			ArrayList<String> lexicalEntries = new ArrayList<String>();		
			// Create a new query
			String queryString = 
				" PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>" +
				" PREFIX lime: <http://www.w3.org/ns/lemon/lime#> " +
			    " SELECT DISTINCT ?lex_entry " +
				" WHERE { " +
				"  ?form ontolex:writtenRep \"" + lemma + "\"@" + sourceLanguage + "." +
				"  ?lex_entry ontolex:lexicalForm ?form . " +
				" <" + lexiconURI + "> lime:entry ?lex_entry ." +
				"}";
			Query query = QueryFactory.create(queryString);
			// Execute the query and obtain results
			QueryExecution qe = QueryExecutionFactory.sparqlService(getSparqlEndpoint(), query);			
			ResultSet results = qe.execSelect();
			//Review results
			for ( ; results.hasNext() ; ) {			
				QuerySolution soln = results.nextSolution() ;
				//extract lexical entries of direct translations
				String translationLexicalEntry = new String(soln.get("lex_entry").toString());				
				lexicalEntries.add(translationLexicalEntry);								
			}			
			// Important - free up resources used running the query
			qe.close();			
			return lexicalEntries;		
	}	
	
	//uncomment for testing
	public static void main(String[] args) {
		ArrayList<String> le_translations = SPARQLSearches.obtainDirectTranslationsFromLexicalEntry("http://linguistic.linkeddata.es/id/apertium/tranSetES-GL", "http://linguistic.linkeddata.es/id/apertium/lexiconES/abaratamiento-NOUN-es");
		for (String le: le_translations)
			System.out.println(le.toString());	
}

}
