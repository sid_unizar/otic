package translation;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
/*
 * This class computes "One Time Inverse Consultation" algorithm to obtain translation pairs between two unconnected languages and prints those translations to a .tsv file
 */

public class IndirectTranslationsExperiment {
	
	final static String SEPARATOR = "\t";

	public void createTranslatablePairsWithScoreFile (String sparqlEndpoint, String sourceLanguage, String pivotLanguage, String targetLanguage, String outputFile){
		
		OneTimeInverseConsultation otic = new OneTimeInverseConsultation();		
		ArrayList<TranslatablePair> translatablePairs = new ArrayList<TranslatablePair>();		
		String sourceLexicon;		
		if(sourceLanguage == "en") sourceLexicon = "http://linguistic.linkeddata.es/id/apertium/lexiconEN";
		else sourceLexicon = SPARQLSearches.obtainLexiconFromLanguage(sourceLanguage);		
		String translationSetURI1 = SPARQLSearches.obtainTranslationSetFromLanguages(sourceLanguage, pivotLanguage);
		String translationSetURI2 = SPARQLSearches.obtainTranslationSetFromLanguages(pivotLanguage, targetLanguage);		
		// Create a new query to get all the source labels along with their associated lexical entries
		System.out.println(sourceLanguage);
		System.out.println(sourceLexicon);
		String queryString = 
			" PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#> " +
			" PREFIX lime: <http://www.w3.org/ns/lemon/lime#> " +
		    " SELECT DISTINCT ?sourceLabel ?lex_entry " + 
			" WHERE { " +
			" <" + sourceLexicon + "> lime:entry ?lex_entry ." + 
			"  ?lex_entry ontolex:lexicalForm ?form . " +
			"  ?form ontolex:writtenRep ?sourceLabel ." +
			"} ";
		Query query = QueryFactory.create(queryString);
		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlEndpoint, query);		
		ResultSet results = qe.execSelect();	
		try {			
			PrintWriter writer = new PrintWriter(outputFile, "UTF-8");	
			//Review results
			for ( ; results.hasNext() ; )    {		
				QuerySolution soln = results.nextSolution() ;
				String sourceLabel = soln.get("sourceLabel").toString();
				String le = soln.get("lex_entry").toString();			
				ArrayList<TranslatablePair> tps = otic.obtainTranslationScoresFromLexicalEntry(sourceLabel.substring(0, sourceLabel.indexOf("@")), le, translationSetURI1, translationSetURI2);	
				for (TranslatablePair tp : tps)
					tp.printToFile(writer);	
				translatablePairs.addAll(tps);		
			}
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		qe.close();	
	
	}		
	
	public static void main(String[] args) {		
		String langS = "pt";
		String langP = "es";
		String langT = "gl";
		String outputFile = "data/OTIC_" + langS + "-" + langT + ".tsv";		
		String sparqlEndpoint = "http://localhost:3030/apertium/query"; //assuming a local SPARQL endpoint with dataset name "apertium"
		//String sparqlEndpoint = "http://linguistic.linkeddata.es/sparql/";
		
		IndirectTranslationsExperiment myExperiment = new IndirectTranslationsExperiment();
		myExperiment.createTranslatablePairsWithScoreFile(sparqlEndpoint, langS , langP, langT, outputFile);	
		
	}

}
